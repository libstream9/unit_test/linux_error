#ifndef STREAM9_LINUX_CORE_TEST_SRC_PATH_HPP
#define STREAM9_LINUX_CORE_TEST_SRC_PATH_HPP

#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace testing {

using st9::string;
using st9::string_view;

string
operator/(string_view x, string_view y)
{
    string v = x;

    v.append('/');
    v.append(y);

    return v;
}

} // namespace testing

#endif // STREAM9_LINUX_CORE_TEST_SRC_PATH_HPP
