#ifndef STREAM9_LINUX_FILE_TEST_SRC_NAMESPACE_HPP
#define STREAM9_LINUX_FILE_TEST_SRC_NAMESPACE_HPP

namespace stream9::filesystem {}
namespace stream9::linux {}
namespace stream9::strings {}
namespace stream9::json {}

namespace testing {

namespace st9 { using namespace stream9; }
namespace fs { using namespace st9::filesystem; }
namespace li { using namespace st9::linux; }
namespace lx { using namespace st9::linux; }
namespace str { using namespace st9::strings; }
namespace json { using namespace st9::json; }

} // namespace testing

#endif // STREAM9_LINUX_FILE_TEST_SRC_NAMESPACE_HPP
