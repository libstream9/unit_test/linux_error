#ifndef STREAM9_LINUX_CORE_TEST_SRC_LOAD_STRING_HPP
#define STREAM9_LINUX_CORE_TEST_SRC_LOAD_STRING_HPP

#include "open.hpp"

#include <stream9/linux/read.hpp>
#include <stream9/string.hpp>
#include <stream9/cstring_ptr.hpp>

namespace testing {

using st9::cstring_ptr;
using st9::string;
using st9::string_view;

static string
load_string(cstring_ptr const& p)
{
    try {
        string s;

        auto fd = open(p, O_RDONLY);

        char buf[1024];
        while (true) {
            auto o_n = li::read(fd, buf);
            if (!o_n) {
                throw st9::error {
                    "read(2)",
                    lx::make_error_code(o_n.error())
                };
            }
            auto n = *o_n;

            if (n == 0) break;

            s.append(string_view(buf, n));
        }

        return s;
    }
    catch (...) {
        st9::rethrow_error();
    }
}

} // namespace testing

#endif // STREAM9_LINUX_CORE_TEST_SRC_LOAD_STRING_HPP
