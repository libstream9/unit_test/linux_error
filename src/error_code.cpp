#include <stream9/linux/error_code.hpp>

#include <system_error>

#include <errno.h>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace lx = stream9::linux;

BOOST_AUTO_TEST_SUITE(error_code_)

    BOOST_AUTO_TEST_CASE(initialize_)
    {
        lx::errc e1 {};
        BOOST_TEST(!e1);

        auto e2 = static_cast<lx::errc>(EPERM);
        BOOST_TEST(e2 == lx::eperm);
    }

    BOOST_AUTO_TEST_CASE(comparison_)
    {
        BOOST_TEST(lx::eperm == EPERM);
        BOOST_TEST(lx::edeadlock == lx::edeadlk);
    }

    BOOST_AUTO_TEST_CASE(conversion_)
    {
        int e1 = lx::eperm;
        BOOST_TEST(e1 == lx::eperm);
    }

    BOOST_AUTO_TEST_CASE(error_code_)
    {
        std::error_code e1 { lx::eperm };

        BOOST_TEST(e1 == lx::eperm);
    }

    BOOST_AUTO_TEST_CASE(compare_with_std_errc_1_)
    {
        std::error_code e1 { lx::eperm };

        BOOST_CHECK(e1 == std::errc::operation_not_permitted);
        BOOST_CHECK(e1 != std::errc::bad_address);
        BOOST_CHECK(e1 != std::errc());
    }

    BOOST_AUTO_TEST_CASE(compare_with_std_errc_2_)
    {
        std::error_code e1 { lx::eagain };
        auto e2 = lx::make_error_code(EWOULDBLOCK);

        BOOST_CHECK(e1 == std::errc::resource_unavailable_try_again);
        BOOST_CHECK(e1 == std::errc::operation_would_block);
        BOOST_CHECK(e1 == e2);
        BOOST_CHECK(e1 != std::errc::bad_address);
    }

    BOOST_AUTO_TEST_CASE(compare_with_bogus_condition_)
    {
        std::error_condition e1 { 200, std::generic_category() };
        std::error_code e2 { 200, lx::error_category() };

        BOOST_CHECK(e1 != e2);
    }

    BOOST_AUTO_TEST_CASE(message_)
    {
        std::error_code e1 { lx::einval };

        BOOST_TEST(e1.message() == "EINVAL: Invalid argument");
    }

BOOST_AUTO_TEST_SUITE_END() // error_code_

} // namespace testing
