#include <stream9/linux/read.hpp>

#include <stream9/linux/write.hpp>

#include "namespace.hpp"
#include "open.hpp"
#include "temporary_file.hpp"

#include <array>
#include <span>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(read_)

    json::array
    to_json(std::span<char> s)
    {
        json::array arr;
        for (auto const c: s) {
            arr.push_back(c);
        }
        return arr;
    }

    BOOST_AUTO_TEST_CASE(array_)
    {
        temporary_file os { "foo" };

        auto fd = open(os.path(), O_RDONLY);

        char buf[256] {};

        auto o_n = lx::read(fd, buf);
        BOOST_REQUIRE(*o_n);
        auto n = *o_n;

        std::span r { buf, n };

        json::array expected { 'f', 'o', 'o' };
        BOOST_TEST(to_json(r) == expected);
    }

    BOOST_AUTO_TEST_CASE(std_array_)
    {
        temporary_file os { "foo" };

        auto fd = open(os.path(), O_RDONLY);

        std::array<char, 256> buf {};

        auto o_n = lx::read(fd, buf);
        BOOST_REQUIRE(*o_n);
        auto n = *o_n;

        std::span r { buf.data(), n };

        json::array expected { 'f', 'o', 'o' };
        BOOST_TEST(to_json(r) == expected);
    }

    BOOST_AUTO_TEST_CASE(vector_)
    {
        temporary_file os { "foo" };

        auto fd = open(os.path(), O_RDONLY);

        std::vector<char> buf; buf.resize(256);

        auto o_n = lx::read(fd, buf);
        BOOST_REQUIRE(*o_n);
        auto n = *o_n;

        std::span r { buf.data(), n };

        json::array expected { 'f', 'o', 'o' };
        BOOST_TEST(to_json(r) == expected);
    }

    BOOST_AUTO_TEST_CASE(int_)
    {
        temporary_file f;

        {
            int i = 1000;
            auto o_n = lx::write(f.fd(), i);
            BOOST_REQUIRE(*o_n);
            auto n = *o_n;

            BOOST_REQUIRE(n == sizeof(int));
        }

        {
            auto r = ::lseek(f.fd(), SEEK_SET, 0); //TODO
            BOOST_REQUIRE(r == 0);
        }

        {
            int i;
            auto o_n = lx::read(f.fd(), i);
            BOOST_REQUIRE(*o_n);
            auto n = *o_n;

            BOOST_REQUIRE(n == sizeof(int));
            BOOST_TEST(i == 1000);
        }
    }

BOOST_AUTO_TEST_SUITE_END() // read_

} // namespace testing
