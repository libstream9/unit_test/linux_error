#ifndef STERAM9_LINUX_CORE_TEST_SRC_OPEN_HPP
#define STERAM9_LINUX_CORE_TEST_SRC_OPEN_HPP

#include "namespace.hpp"

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>

#include <fcntl.h>

namespace testing {

inline lx::fd
open(st9::cstring_ptr const& pathname, int flags)
{
    auto rc = ::open(pathname, flags, 0666);
    if (rc == -1) {
        throw st9::error {
            "open(2)",
            lx::make_error_code(errno),
        };
    }
    else {
        return lx::fd(rc);
    }
}

} // namespace testing

#endif // STERAM9_LINUX_CORE_TEST_SRC_OPEN_HPP
