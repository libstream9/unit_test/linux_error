#ifndef STREAM9_LINUX_CORE_TEST_SRC_TEMPORARY_FILE_HPP
#define STREAM9_LINUX_CORE_TEST_SRC_TEMPORARY_FILE_HPP

#include "namespace.hpp"

#include <stream9/cstring_ptr.hpp>
#include <stream9/string.hpp>
#include <stream9/linux/fd.hpp>

#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

namespace testing {

using st9::string;
using st9::cstring_ptr;

class temporary_file
{
public:
    temporary_file()
        : m_path { "/tmp/XXXXXX" }
        , m_fd { ::mkstemp(m_path.data()) }
    {}

    temporary_file(cstring_ptr const& text)
        : temporary_file()
    {
        lx::write(m_fd, text);
    }

    ~temporary_file() noexcept
    {
        ::unlink(m_path.c_str());
    }

    auto const& fd() const noexcept { return m_fd; }
    auto const& path() const noexcept { return m_path; }

private:
    string m_path;
    li::fd m_fd;
};

} // namespace testing

#endif // STREAM9_LINUX_CORE_TEST_SRC_TEMPORARY_FILE_HPP
