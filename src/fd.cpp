#include <stream9/linux/fd.hpp>

#include "namespace.hpp"
#include "open.hpp"
#include "path.hpp"
#include "temporary_directory.hpp"

#include <concepts>
#include <optional>

#include <fcntl.h>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(fd_)

    BOOST_AUTO_TEST_CASE(concept_)
    {
        static_assert(std::movable<li::fd>);
        static_assert(std::totally_ordered<li::fd>);
    }

    BOOST_AUTO_TEST_CASE(close_on_destruction_)
    {
        temporary_directory d;

        auto path = d.path() / "foo";

        std::optional fd = open(path, O_CREAT);

        auto result = ::fcntl(*fd, F_GETFD); // check if fd is active
        BOOST_REQUIRE(result != -1);

        int f = *fd;
        fd.reset();

        result = ::fcntl(f, F_GETFD);
        BOOST_REQUIRE(result == -1);
        BOOST_REQUIRE(errno == EBADF);
    }

    BOOST_AUTO_TEST_CASE(descriptor_flags_)
    {
        temporary_directory d;

        auto path = d.path() / "foo";

        auto fd = open(path, O_RDWR | O_CREAT | O_CLOEXEC);

        BOOST_TEST(fd.descriptor_flags() == FD_CLOEXEC);

        fd.set_descriptor_flags(0);
        BOOST_TEST(fd.descriptor_flags() == 0);
    }

    BOOST_AUTO_TEST_CASE(status_flags_)
    {
        temporary_directory d;

        auto path = d.path() / "foo";

        auto fd = open(path, O_RDWR | O_CREAT | O_CLOEXEC);

        BOOST_TEST(fd.status_flags() & O_RDWR);

        fd.set_status_flags(0);
        BOOST_TEST((fd.status_flags() & (O_CREAT | O_CLOEXEC)) == 0);
    }

    BOOST_AUTO_TEST_CASE(close_on_exec_)
    {
        temporary_directory d;

        auto path = d.path() / "foo";

        auto fd = open(path, O_RDWR | O_CREAT | O_CLOEXEC);

        BOOST_TEST(fd.close_on_exec());

        fd.set_close_on_exec(false);
        BOOST_TEST(!fd.close_on_exec());

        fd.set_close_on_exec(true);
        BOOST_TEST(fd.close_on_exec());
    }

    BOOST_AUTO_TEST_CASE(nonblocking_mode_)
    {
        temporary_directory d;

        auto path = d.path() / "foo";

        auto fd = open(path, O_RDWR | O_CREAT | O_CLOEXEC);

        BOOST_TEST(!fd.nonblocking_mode());

        fd.set_nonblocking_mode(true);
        BOOST_TEST(fd.nonblocking_mode());

        fd.set_nonblocking_mode(false);
        BOOST_TEST(!fd.nonblocking_mode());
    }

    BOOST_AUTO_TEST_CASE(path_)
    {
        temporary_directory d;

        auto path = d.path() / "foo";

        auto fd = open(path, O_RDWR | O_CREAT);

        BOOST_TEST(fd.path() == path);
    }

BOOST_AUTO_TEST_SUITE_END() // fd_

BOOST_AUTO_TEST_SUITE(fd_ref_)

    BOOST_AUTO_TEST_CASE(construct_)
    {
        temporary_directory d;

        auto path = d.path() / "foo";

        auto f = open(path, O_CREAT);

        li::fd_ref r { f };

        BOOST_TEST(f == r);
    }

    BOOST_AUTO_TEST_CASE(path_)
    {
        temporary_directory d;

        auto path = d.path() / "foo";

        auto fd = open(path, O_RDWR | O_CREAT);

        li::fd_ref r { fd };

        BOOST_TEST(r.path() == path);
    }

    BOOST_AUTO_TEST_CASE(close_on_exec_)
    {
        temporary_directory d;

        auto path = d.path() / "foo";

        auto fd = open(path, O_RDWR | O_CREAT | O_CLOEXEC);
        lx::fd_ref r { fd };

        BOOST_TEST(r.close_on_exec());

        r.set_close_on_exec(false);
        BOOST_TEST(!r.close_on_exec());

        r.set_close_on_exec(true);
        BOOST_TEST(r.close_on_exec());
    }

    BOOST_AUTO_TEST_CASE(nonblocking_mode_)
    {
        temporary_directory d;

        auto path = d.path() / "foo";

        auto fd = open(path, O_RDWR | O_CREAT | O_CLOEXEC);
        lx::fd_ref r { fd };

        BOOST_TEST(!r.nonblocking_mode());

        r.set_nonblocking_mode(true);
        BOOST_TEST(r.nonblocking_mode());

        r.set_nonblocking_mode(false);
        BOOST_TEST(!r.nonblocking_mode());
    }

BOOST_AUTO_TEST_SUITE_END() // fd_ref_

} // namespace testing
