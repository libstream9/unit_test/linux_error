#include <stream9/linux/write.hpp>

#include "load_string.hpp"
#include "namespace.hpp"
#include "open.hpp"
#include "temporary_file.hpp"

#include <array>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(write_)

    BOOST_AUTO_TEST_CASE(array_)
    {
        temporary_file f;

        auto fd = open(f.path(), O_WRONLY);

        char buf[] { 'f', 'o', 'o', };

        auto o_n = lx::write(fd, buf);
        BOOST_REQUIRE(o_n.has_value());
        auto n = *o_n;

        BOOST_TEST(n == 3);

        auto s = load_string(f.path());
        BOOST_TEST_REQUIRE(s.size() == 3);
        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(std_array_)
    {
        temporary_file f;

        auto fd = open(f.path(), O_WRONLY);

        std::array<char, 3> buf { 'f', 'o', 'o', };

        auto o_n = lx::write(fd, buf);
        BOOST_REQUIRE(o_n.has_value());
        auto n = *o_n;

        BOOST_TEST(n == 3);

        auto s = load_string(f.path());
        BOOST_TEST_REQUIRE(s.size() == 3);
        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_)
    {
        temporary_file f;

        auto fd = open(f.path(), O_WRONLY);

        std::string buf = "foo";

        auto o_n = lx::write(fd, buf);
        BOOST_REQUIRE(o_n.has_value());
        auto n = *o_n;

        BOOST_TEST(n == 3);

        auto s = load_string(f.path());
        BOOST_TEST_REQUIRE(s.size() == 3);
        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        temporary_file f;

        auto fd = open(f.path(), O_WRONLY);

        std::string buf = "";

        auto o_n = lx::write(fd, buf);
        BOOST_REQUIRE(o_n.has_value());
        auto n = *o_n;

        BOOST_TEST(n == 0);

        auto s = load_string(f.path());
        BOOST_TEST_REQUIRE(s.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(error_1_)
    {
        std::string buf {};

        auto o_n = lx::write(-1, buf);

        BOOST_REQUIRE(!o_n);
        BOOST_TEST(o_n.error() == lx::ebadf);
    }

BOOST_AUTO_TEST_SUITE_END() // write_

} // namespace testing
