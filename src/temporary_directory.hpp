#ifndef LINUX_CORE_TEST_SRC_TEMPORARY_DIRECTORY_HPP
#define LINUX_CORE_TEST_SRC_TEMPORARY_DIRECTORY_HPP

#include "namespace.hpp"

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

#include <dirent.h>
#include <fcntl.h>
#include <sys/types.h>

namespace testing {

using st9::cstring_ptr;
using st9::string;
using st9::string_view;

static DIR*
opendirat(int fd, cstring_ptr const& p) noexcept
{
    auto fd2 = ::openat(fd, p, O_RDONLY | O_CLOEXEC);
    if (fd2 == -1) {
        std::cerr << "fali on openat(" << p << ")" << std::endl;
        return nullptr;
    }

    auto* dir = ::fdopendir(fd);
    if (dir == nullptr) {
        std::cerr << "fail on fdopendir(" << p << ")" << std::endl;
        return nullptr;
    }

    return dir;
}

static void
remove_recursive(int fd, cstring_ptr const& p) noexcept
{
    auto rc = ::unlinkat(fd, p, 0);
    if (rc != -1) return;

    if (errno == EISDIR) {
        auto* dir = opendirat(fd, p);
        if (dir == nullptr) return;

        while (auto* ent = ::readdir(dir)) {
            auto* name = ent->d_name;
            if (::strcmp(name, ".") == 0 || ::strcmp(name, "..") == 0) continue;
            remove_recursive(::dirfd(dir), name);
        }

        rc = ::rmdir(p);
        if (rc == -1) {
            std::cerr << "fail on rmdir(" << p << ")" << std::endl;
        }
    }
    else {
        std::cerr << "fail on unlink(" << p << ")" << std::endl;
    }
}

static void
remove_recursive(cstring_ptr const& p) noexcept
{
    auto rc = ::unlink(p);
    if (rc != -1) return;

    if (errno == EISDIR) {
        auto* dir = ::opendir(p);
        if (dir == nullptr) return;

        while (auto* ent = ::readdir(dir)) {
            auto* name = ent->d_name;
            if (::strcmp(name, ".") == 0 || ::strcmp(name, "..") == 0) continue;
            remove_recursive(::dirfd(dir), name);
        }

        rc = ::rmdir(p);
        if (rc == -1) {
            std::cerr << "fail on rmdir(" << p << ")" << std::endl;
        }
    }
    else {
        std::cerr << "fail on unlink(" << p << ")" << std::endl;
    }
}

class temporary_directory
{
public:
    temporary_directory()
    {
        m_path = "/tmp/XXXXXX";
        if (::mkdtemp(m_path.data()) == nullptr) {
            throw st9::error {
                "mkdtemp(3)",
                li::make_error_code(errno),
            };
        }
    }

    ~temporary_directory() noexcept
    {
        try {
            remove_recursive(m_path);
        }
        catch (...) {
            st9::print_error();
        }
    }

    string_view path() const noexcept { return m_path; }

private:
    string m_path;
};

} // namespace testing

#endif // LINUX_CORE_TEST_SRC_TEMPORARY_DIRECTORY_HPP
