#include <stream9/linux/readv.hpp>

#include "namespace.hpp"

#include <stream9/array_view.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using st9::array_view;
using st9::string;
using st9::string_view;

using namespace st9::literals;

inline auto
pipe() noexcept
{
    int pfd[2];
    auto rc = ::pipe(pfd);
    BOOST_REQUIRE(rc != -1);

    return std::make_pair(
        lx::fd(pfd[0]), lx::fd(pfd[1])
    );
}

BOOST_AUTO_TEST_SUITE(readv_)

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        try {
            auto pfd = pipe();

            string_view msg = "foobar\nbazhello";
            auto n = ::write(pfd.second, msg.data(), msg.size());
            BOOST_REQUIRE(n == 15);

            char buf1[10];
            char buf2[10];
            lx::iovec read_iov[] = {
                { buf1, sizeof(buf1) },
                buf2
            };

            auto m = lx::readv(pfd.first, read_iov).or_throw();

            BOOST_REQUIRE(m == 15);
            BOOST_TEST(string_view(buf1, 10) == "foobar\nbaz");
            BOOST_TEST(string_view(buf2, 5) == "hello");
        }
        catch (...) {
            st9::print_error();
        }
    }

BOOST_AUTO_TEST_SUITE_END() // readv_

} // namespace testing
