#include <stream9/linux/writev.hpp>

#include "namespace.hpp"

#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using st9::string;
using st9::string_view;

using namespace st9::literals;

inline auto
pipe() noexcept
{
    int pfd[2];
    auto rc = ::pipe(pfd);
    BOOST_REQUIRE(rc != -1);

    return std::make_pair(
        lx::fd(pfd[0]), lx::fd(pfd[1])
    );
}

BOOST_AUTO_TEST_SUITE(writev_)

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        try {
            auto pfd = pipe();

            string_view delim = "\n";
            string s = "hello";
            lx::const_iovec iov[] = {
                { "foo", 3 },
                "bar",
                delim,
                "baz"sv,
                s
            };

            auto n = lx::writev(pfd.second, iov).or_throw();

            BOOST_TEST(n == 15);
        }
        catch (...) {
            st9::print_error();
        }
    }

BOOST_AUTO_TEST_SUITE_END() // writev_

} // namespace testing
